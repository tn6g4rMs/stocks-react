import { useState, useEffect } from "react";
import axios from "axios";
import Navbar from "./components/Navbar";
import Table from "./components/Table";
import SelectCompany from "./components/SelectCompany";
import Stock from "./components/Stock";

function App() {
  const [stocks, setStocks] = useState([]);
  const [symbol, setSymbol] = useState(null);
  const [selectedIndexCompany, setSelectedIndexCompany] = useState(null);
  const [companies, setCompanies] = useState(
    JSON.parse(localStorage.getItem("companies"))
  );

  const getSymbol = (event) => {
    setSymbol(event.target.value);
  };

  const getStocks = async () => {
    // URL to get new key https://marketstack.com/signup
    // only 500 requests/month
    const key = "e3c59274c0e149fac5e64c138df3d19d";
    const link = `http://api.marketstack.com/v1/eod?access_key=${key}&symbols=${symbol}&limit=10`;

    axios
      .get(link)
      .then((res) => {
        // console.log(res);
        if (res.status === 200) {
          setStocks(res.data.data);
        } else {
          setStocks([]);
        }
      })
      .catch((err) => {
        setStocks([]);
      });
  };

  const addCompany = (company) => {
    const upperCaseName =
      company.name.charAt(0).toUpperCase() + company.name.slice(1);
    const upperCaseSymbol = company.symbol.toUpperCase();

    const newCompany = { name: upperCaseName, symbol: upperCaseSymbol };

    let LsCompanies = [];

    if (localStorage.getItem("companies") === null) {
      LsCompanies.push(newCompany);
      localStorage.setItem("companies", JSON.stringify(LsCompanies));

      setCompanies(JSON.parse(localStorage.getItem("companies")));
    } else {
      LsCompanies = JSON.parse(localStorage.getItem("companies"));
      LsCompanies.unshift(newCompany);
      localStorage.setItem("companies", JSON.stringify(LsCompanies));

      setCompanies(JSON.parse(localStorage.getItem("companies")));
    }

    alert("Company Added");
  };

  const deleteCompany = () => {
    let LsCompanies = [];

    if (
      localStorage.getItem("companies") !== null &&
      selectedIndexCompany !== null
    ) {
      LsCompanies = JSON.parse(localStorage.getItem("companies"));
      LsCompanies.splice(selectedIndexCompany, 1);

      localStorage.setItem("companies", JSON.stringify(LsCompanies));

      setCompanies(JSON.parse(localStorage.getItem("companies")));

      alert("Company Deleted");
    }
  };

  useEffect(() => {
    if (companies !== null && companies.length > 0) {
      setSymbol(companies[0].symbol);

      return;
    }
    setStocks([]);
  }, [companies]);

  return (
    <div>
      <Navbar addCompany={addCompany} />

      <div className="container my-4">
        <div className="row ">
          <div className="col-12 col-md-6 ">
            <SelectCompany
              getSymbol={getSymbol}
              companies={companies}
              getStocks={getStocks}
              deleteCompany={deleteCompany}
              setSelectedIndexCompany={setSelectedIndexCompany}
            />
          </div>
          <div className="col-12 col-md-6 float-right">
            {stocks.length > 0 ? (
              <Stock stocks={stocks} />
            ) : (
              <h4 className="text-center my-3">Company not found</h4>
            )}
          </div>
          <div className="col-12 col-md-3"></div>
        </div>

        {stocks.length > 0 ? <Table stocks={stocks} /> : ""}
      </div>
    </div>
  );
}

export default App;
