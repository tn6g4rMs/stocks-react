import React, { useState } from "react";

const AddCompany = ({ addCompany }) => {
  const [newCompany, setNewCompany] = useState({ name: "", symbol: "" });

  const addNewCompany = () => {
    if (newCompany.name === "" || newCompany.symbol === "") {
      alert("Add Both A Company Name And A Company Symbol");

      return;
    }
    addCompany(newCompany);
    setNewCompany({ name: "", symbol: "" });
  };

  return (
    <>
      {/* <!-- Button trigger modal --> */}
      <button
        className="btn btn-outline-dark"
        type="button"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
      >
        Add Company
      </button>
      {/* <!-- Button trigger modal --> */}

      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Add a new company
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <label htmlFor="name">Company Symbol</label>
              <input
                value={newCompany.name}
                type="text"
                className="form-control my-2"
                id="name"
                onChange={(event) =>
                  setNewCompany({ ...newCompany, name: event.target.value })
                }
              />
              <label htmlFor="symbol">Company Symbol</label>
              <input
                type="text"
                className="form-control my-2"
                id="symbol"
                value={newCompany.symbol}
                onChange={(event) =>
                  setNewCompany({ ...newCompany, symbol: event.target.value })
                }
              />
            </div>
            <div className="modal-footer">
              <button
                onClick={addNewCompany}
                type="button"
                className="btn btn-primary"
                data-bs-dismiss={
                  newCompany.name === "" && newCompany.symbol === ""
                    ? null
                    : "modal"
                }
              >
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
      {/*  */}
    </>
  );
};

export default AddCompany;
