import React from "react";
import AddCompany from "./AddCompany";

function Navbar({ addCompany }) {
  return (
    <nav className="navbar navbar-light ">
      <div className="container">
        <h2 className="navbar-brand">Stocks React App</h2>

        <AddCompany addCompany={addCompany} />
      </div>
    </nav>
  );
}

export default Navbar;
