import React from "react";
import deleteIcon from "../assets/deleteIcon.png";

const SelectCompany = (props) => {
  const {
    getSymbol,
    companies,
    getStocks,
    deleteCompany,
    setSelectedIndexCompany,
  } = props;

  return (
    <div>
      <div className="d-flex">
        <select
          className="form-select mb-3"
          onChange={getSymbol}
          onClick={(event) =>
            setSelectedIndexCompany(event.target.options.selectedIndex)
          }
        >
          {companies !== null && companies.length > 0 ? (
            companies.map((company, index) => {
              return (
                <option key={index} value={company.symbol}>
                  {company.name}
                </option>
              );
            })
          ) : (
            <option disabled>No Company, Add One</option>
          )}
        </select>
        <div style={{ width: "10px" }}> </div>

        <img
          className="mt-2"
          src={deleteIcon}
          alt="delete"
          style={{ width: "23px", height: "23px", cursor: "pointer" }}
          onClick={
            companies.length > 0
              ? deleteCompany
              : () => alert("Nothing to delete")
          }
        />
      </div>
      <button
        style={{ width: "100%" }}
        type="button"
        onClick={getStocks}
        className="btn btn-outline-dark mb-3"
      >
        Get Stocks
      </button>
    </div>
  );
};

export default SelectCompany;
