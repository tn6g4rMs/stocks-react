import React, { useState, useEffect } from "react";

const Stock = ({ stocks }) => {
  const [isPositive, setIsPositive] = useState(true);
  const [number, setNumber] = useState(null);
  const todayStockPrice = stocks[0];
  const dayBeforeTodayStockPrice = stocks[1];

  useEffect(() => {
    const int = todayStockPrice.close - dayBeforeTodayStockPrice.close;
    const final = int.toFixed(2);
    setNumber(final);

    if (final.slice(0, 1) === "-") {
      setIsPositive(false);
    } else {
      setIsPositive(true);
    }
  }, [todayStockPrice.close, dayBeforeTodayStockPrice.close]);

  return (
    <div className="stock-container">
      <h4 className="text-center mb-3">EOD Stock Price</h4>
      <div className="d-flex justify-content-between ">
        <div>
          <h3>{todayStockPrice.symbol}</h3>
        </div>
        <div>
          <p>
            High: <span className="high">{todayStockPrice.high}</span>
          </p>
          <p>
            Low: <span className="low">{todayStockPrice.low}</span>
          </p>
        </div>
        <div className="text-end">
          <p className="close">{todayStockPrice.close}</p>
          <div className={isPositive ? "positive" : "negative"}>
            {isPositive ? "+" : ""}
            {number}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Stock;
