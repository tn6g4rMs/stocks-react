import React from "react";

const Table = ({ stocks }) => {
  return (
    <div className="my-3 table-container">
      <h3 className="text-center">Last 10 EOD Stock Prices</h3>
      <table className="table mt-4">
        <thead>
          <tr>
            <th scope="col">Date</th>
            <th scope="col">Open</th>
            <th scope="col">High</th>
            <th scope="col">Low</th>
            <th scope="col">Close</th>
            <th scope="col">Volume</th>
          </tr>
        </thead>
        <tbody>
          {stocks.map((stock) => {
            return (
              <tr key={stock.date}>
                <th scope="row">{stock.date.slice(0, 10)}</th>
                <td>{stock.open}</td>
                <td className="high">{stock.high}</td>
                <td className="low">{stock.low}</td>
                <td>{stock.close}o</td>
                <td>{stock.volume}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
